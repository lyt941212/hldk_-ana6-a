import os
import time
import ffmpeg
from PyQt5.uic import loadUiType
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets
import sys
import cv2
import numpy as np
import socket
convert_ui, _ = loadUiType("test.ui")


class main_ui(QMainWindow, convert_ui):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.handle_buttons()
        self.count1 = 0
        self.count2 = 0
        self.count3 = 0
        self.timer_camera = QTimer()
        self.label_3.setText('服务器已启动，等待客户端连接...')
        self.alpha_rtsp = f"rtsp://192.168.1.33:8554/stream"
        self.args = {
            "rtsp_transport": "tcp",
            "fflags": "nobuffer",
            "flags": "low_delay"
        }
        self.save_count = 0
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def handle_buttons(self):
        self.pushButton_select.clicked.connect(self.read_pics1)
        self.pushButton_select_2.clicked.connect(self.read_pics2)
        self.pushButton_select_3.clicked.connect(self.read_pics3)
        self.pushButton.clicked.connect(self.get_rtsp)
        self.pushButton_quit.clicked.connect(self.look_fire_start)
        self.pushButton_2.clicked.connect(self.send_req1)
        self.pushButton_3.clicked.connect(self.send_req2)
        self.pushButton_4.clicked.connect(self.send_req3)

    def look_fire_start(self):
        self.backbend = udpConnect()
        self.backbend.start()
        self.backbend.getdata.connect(self.look_fire)

    def look_fire(self, data):
        if data == "1":
            self.label_3.setText("1号探头报警")
        elif data == "2":
            self.label_3.setText("2号探头报警")
        elif data == "3":
            self.label_3.setText("3号探头报警")

    def read_pics1(self):
        self.mag1_list = os.listdir("pics/MAG1/")
        self.mag1_list.sort(key=lambda x: int(x.split(".")[0]))
        self.len_num1 = len(self.mag1_list)
        self.timer_camera.start(100)
        self.timer_camera.timeout.connect(self.open_pics1)

    def read_pics2(self):
        self.mag2_list = os.listdir("pics/MAG2/")
        self.mag2_list.sort(key=lambda x: int(x.split(".")[0]))
        self.len_num2 = len(self.mag2_list)
        self.timer_camera.start(200)
        self.timer_camera.timeout.connect(self.open_pics2)

    def read_pics3(self):
        self.mag3_list = os.listdir("pics/MAG3/")
        self.mag3_list.sort(key=lambda x: int(x.split(".")[0]))
        self.len_num1 = len(self.mag3_list)
        self.timer_camera.start(100)
        self.timer_camera.timeout.connect(self.open_pics3)

    def send_req1(self, data):
        self.label_2.setText("开始获取探头1，请等待....")
        self.backbend.sendControlMag1()
        self.path = "pics/MAG1/"

    def send_req2(self):
        self.label_2.setText("开始获取探头2，请等待....")
        self.backbend.sendControlMag2()
        self.path = "pics/MAG2/"

    def send_req3(self):
        self.label_2.setText("开始获取探头3，请等待....")
        self.backbend.sendControlMag3()
        self.path = "pics/MAG3/"

    def get_rtsp(self):
        probe = ffmpeg.probe(self.alpha_rtsp)
        cap_info = next(x for x in probe['streams'] if x['codec_type'] == 'video')
        width = cap_info['width']  # 获取视频流的宽度
        height = cap_info['height']  # 获取视频流的高度
        self.process1 = (
            ffmpeg
            .input(self.alpha_rtsp, **self.args)
            .output('pipe:', format='rawvideo', pix_fmt='rgb24')
            .overwrite_output()
            .run_async(pipe_stdout=True)
        )

        while True:
            in_bytes = self.process1.stdout.read(width * height * 3)  # 读取图
            if not in_bytes:
                break
            # 转成ndarray
            in_frame = (
                np
                .frombuffer(in_bytes, np.uint8)
                .reshape([height, width, 3])
            )
            # in_frame = cv2.resize(in_frame, (1280, 720))   # 改变图片尺寸
            frame = cv2.cvtColor(in_frame, cv2.COLOR_RGB2BGR)  # 转成BGR
            cv2.imwrite(self.path + str(self.save_count) + '.jpg', frame)
            self.save_count += 1
        self.label_2.setText("获取完毕")
        self.save_count = 0
        self.process1.kill()

    def open_pics1(self):
        pix = QPixmap("pics/MAG1" + "\\" + self.mag1_list[self.count1])
        self.label_2.setPixmap(pix)
        self.label_2.setScaledContents(True)
        self.count1 += 1
        if (self.count1 == self.len_num1):
            self.count1 = 0
            self.timer_camera.stop()

    def open_pics2(self):
        pix = QPixmap("pics/MAG2" + "\\" + self.mag2_list[self.count2])
        self.label_2.setPixmap(pix)
        self.label_2.setScaledContents(True)
        self.count2 += 1
        if (self.count2 == self.len_num2):
            self.count2 = 0
            self.timer_camera.stop()

    def open_pics3(self):
        pix = QPixmap("pics/MAG3" + "\\" + self.mag3_list[self.count3])
        self.label_2.setPixmap(pix)
        self.label_2.setScaledContents(True)
        self.count3 += 1
        if (self.count3 == self.len_num):
            self.count3 = 0
            self.timer_camera.stop()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, "退出", "是否要退出程序?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.close()
            event.accept()
        else:
            event.ignore()


class udpConnect(QThread):
    getdata = pyqtSignal(str)

    def __init__(self):
        super(udpConnect, self).__init__()

        self.save_count = 0
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # 设置IP地址和端口号
        self.server_address = ('192.168.1.131', 2563)
        self.server_socket.bind(self.server_address)

    def run(self):
        while True:
            data, self.client_address = self.server_socket.recvfrom(1024)
            self.copyData = data.decode()
            if (data.decode() == "1"):
                self.getdata.emit("1")
            if (data.decode() == "2"):
                self.getdata.emit("2")
            if (data.decode() == "3"):
                self.getdata.emit("3")

    def sendControlMag1(self):
        if (self.copyData == "1"):
            self.server_socket.sendto("11".encode(), self.client_address)

    def sendControlMag2(self):
        if (self.copyData == "2"):
            self.server_socket.sendto("22".encode(), self.client_address)

    def sendControlMag3(self):
        if (self.copyData == "3"):
            self.server_socket.sendto("33".encode(), self.client_address)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = main_ui()
    window.show()
    sys.exit(app.exec_())
